<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
define('BASEPATH', true);
function error($code = 1000){
    http_response_code($code);
    echo '<pre>';
    if($code == 900){
        return 'Error Code '.$code.': TeamSpeak3 framework or config not loaded!';
    }elseif($code == 951){
        return 'Error Code '.$code.': Needs php version 7.0 and higher!';
    }elseif($code == 952){
        return 'Error Code '.$code.': The new version (<b>'.@json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['version'].'</b>) is on the release! Download at <b><a target="_blank" href="'.@json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['project'].'">'.@json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['project'].'</a></b>';
    }elseif($code == 953){
        return 'Error Code '.$code.': ImageTTF function not exists!';
    }elseif($code == 954){
        return 'Error Code '.$code.': Allow_url_fopen config not enabled!';
    }elseif($code == 955){
        return 'Error Code '.$code.': Curl function not exists!';
    }elseif($code == 956){
        return 'Error Code '.$code.': You are not connected to server!';
    }elseif($code == 957){
        return 'Error Code '.$code.': You are using proxy!';
    }elseif($code == 958){
        return 'Error Code '.$code.': FSockOpen function not exists!';
    }elseif($code == 959){
        return 'Error Code '.$code.': The new version (<b>'.@json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['dev_version'].' - Development</b>) is on the release! Download at <b><a target="_blank" href="'.@json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['project'].'">'.@json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['project'].'</a></b>';
    }else{
        return 'Error Code '.$code.': Unknown error!';
    }
    echo '</pre>';
}
function get_weather($a, $b, $c){
    @preg_match_all('/'.preg_quote($a, '/').'(.*?)'.preg_quote($b,'/').'/i', $c, $s);
    return @$s[1];
}
if ((float)phpversion() < 7.0){
    die(error(951));
}else{
    if(!function_exists('imagettftext')){
        die(error(953));
    }else{
        if(!ini_get('allow_url_fopen')){
            die(error(954));
        }else{
            if(!function_exists('curl_init')){
                die(error(955));
            }else{
                if(file_exists('./vendor/autoload.php') || file_exists('./vendor/config.php')){
                    require_once('./vendor/autoload.php');
                    if(!CONFIG['development']['status'] && APP_VERSION < @json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['version']){
                        die(error(952));
                    }else{
                        if(CONFIG['development']['status'] && APP_DEV_VERSION < @json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['dev_version']){
                            die(error(959));
                        }else{
                            function get_client_ip(){
                                $ipaddress = '';
                                if (isset($_SERVER['HTTP_CLIENT_IP']))
                                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                                else if(isset($_SERVER['HTTP_FORWARDED']))
                                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                                else if(isset($_SERVER['REMOTE_ADDR']))
                                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                                else
                                    $ipaddress = 'UNKNOWN';
                                return $ipaddress;
                            }
                            if(CONFIG['proxy_check']){
                                if(!function_exists('fsockopen')){
                                    die(error(958));
                                }else{
                                    $ports = array(80,8080,6588,8000,3128,3127,3124,1080,553,554,22,3389);
                                    for ($i = 0; $i <= count($ports); $i++){
                                        if (@fsockopen(get_client_ip(), $ports[$i], $errstr, $errno, 0.5)){
                                            die(error(957));
                                        }
                                    }
                                }
                            }else{
                                try{
                                    $ts3 = TeamSpeak3::factory('serverquery://'.CONFIG['username'].':'.CONFIG['password'].'@'.CONFIG['host'].':'.CONFIG['query_port'].'/?server_port='.CONFIG['server_port'].'&blocking=0&nickname='.urlencode(CONFIG['bot_username'].'_'.rand(100000,999999)));
                                    if(CONFIG['server_tracker']){
                                        $ch = curl_init();
                                        curl_setopt($ch, CURLOPT_URL, @json_decode(file_get_contents(base64_decode('aHR0cHM6Ly9naXRsYWIuY29tL2JsYXppbmdsYXllci9wcml2YXRlL3Jhdy9tYXN0ZXIvdGVhbXNwZWFrLWJhbm5lci5jb25maWcucGhw')), true)['tracker']);
                                        curl_setopt($ch, CURLOPT_POST, true);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, 'host='.CONFIG['host'].'&port='.CONFIG['server_port'].'&query_port='.CONFIG['query_port']);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
                                        curl_exec($ch);
                                        curl_close($ch);
                                    }
                                    $username = '';
                                    foreach($ts3->clientList(array('connection_client_ip' => get_client_ip())) as $client){
                                        $username = $client;
                                    }
                                    if(!$username || strlen($username) == 0 || $username == '' || $username == ' '){
                                        die(error(956));
                                    }else{
                                        $ipinfo = @json_decode(file_get_contents('http://ip-api.com/json/'.get_client_ip()), true);
                                        date_default_timezone_set(@$ipinfo['timezone']);
                                        $image = imagecreatefrompng(CONFIG['banners'][rand(0, sizeof(CONFIG['banners'])-1)]);
                                        imagettftext($image, 17, 0, 430, 150, imagecolorallocate($image, 255, 255, 255), CONFIG['fonts']['normal'], 'Welcome '.@$username.'!');
                                        imagettftext($image, 14, 0, 23, 115, imagecolorallocate($image, 82, 82, 82), CONFIG['fonts']['normal'], strftime('%e %B %Y'));
                                        imagettftext($image, 33, 0, 25, 165, imagecolorallocate($image, 82, 82, 82), CONFIG['fonts']['normal'], date('H:i'));
                                        imagettftext($image, 29, 0, 60, 237, imagecolorallocate($image, 82, 82, 82), CONFIG['fonts']['normal'], $ts3['virtualserver_clientsonline'].'/'.$ts3['virtualserver_maxclients']);
                                        if(CONFIG['weather']['apixu']['status'] && CONFIG['weather']['apixu']['api_key'] && !CONFIG['weather']['weathermap']['status']){
                                            $json = @json_decode(file_get_contents('http://api.apixu.com/v1/current.json?key='.CONFIG['weather']['apixu']['api_key'].'&q='.($ipinfo['city'] ? $ipinfo['city'] : 'Ankara')), true);
                                            if(@$json['location']['name']){
                                                imagettftext($image, 15, 0, 690, 240, imagecolorallocate($image, 255, 255, 255), CONFIG['fonts']['normal'], $json['location']['name']);
                                                imagettftext($image, 10, 0, 765, 220, imagecolorallocate($image, 255, 255, 255), CONFIG['fonts']['normal'], $json['current']['temp_c'].'°');
                                                imagecopy($image, imagecreatefrompng(str_replace('//', 'http://', $json['current']['condition']['icon'])), imagesx($image) - imagesx(imagecreatefrompng(str_replace('//', 'http://', $json['current']['condition']['icon']))) - 35, imagesy($image) - imagesy(imagecreatefrompng(str_replace('//', 'http://', $json['current']['condition']['icon']))) - 25, 0, 0, imagesx(imagecreatefrompng(str_replace('//', 'http://', $json['current']['condition']['icon']))), imagesy(imagecreatefrompng(str_replace('//', 'http://', $json['current']['condition']['icon']))));
                                            }
                                        }elseif(CONFIG['weather']['weathermap']['status'] && !CONFIG['weather']['apixu']['status'] && (@json_decode(file_get_contents('http://ip-api.com/json/'.get_client_ip()), true)['city'])){
                                            imagettftext($image, 15, 0, 690, 240, imagecolorallocate($image, 255, 255, 255), CONFIG['fonts']['normal'], @json_decode(file_get_contents('http://ip-api.com/json/'.get_client_ip()), true)['city']);
                                            imagettftext($image, 10, 0, 765, 220, imagecolorallocate($image, 255, 255, 255), CONFIG['fonts']['normal'], get_weather('width=80 height=80><div class=h2>','</div><p>', file_get_contents('https://www.timeanddate.com/weather/'.($ipinfo['country'] ? $ipinfo['country'] : 'Turkey').'/'.($ipinfo['city'] ? $ipinfo['city'] : 'Ankara')))[0]);
                                        }
                                        header('Content-type: image-png; charset=utf-8');
                                        imagepng($image);
                                        imagedestroy($image);
                                    }
                                }catch(TeamSpeak3_Exception $e){
                                    die('<pre>Error Code '.$e->getCode().': '.$e->getMessage().'</pre>');
                                }
                            }
                        }
                    }
                }else{
                    die(error(900));
                }
            }
        }
    }
}
?>